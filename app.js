let app = angular.module('movieLibraryApp', ['ngRoute']);

app.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'Views/MovieList.html',
            controller: "listMovieController"
        })

        .when('/movie', {
            templateUrl: 'Views/ShowMovie.html',
            controller: "movieController"
        })

        .otherwise({redirectTo: '/'});
});

app.controller("listMovieController", ["$scope", "MovieService", "LanguageService", function ($scope, MovieService, LanguageService) {
    this.service = LanguageService;
    MovieService.getAllMovies().then(function (data) {
        $scope.movies = data;
        $scope.movies.forEach(function (item) {
            let notes = MovieService.getNotes(item.imdbID);
            let total = 0;
            let count = 0;
            if (notes != null) {
                notes.forEach(function (note) {
                    total += note.note;
                    count++;
                });
                item.AverageNote = (total / count).toFixed(2);
            } else {
                item.AverageNote = 0;
            }
        });
    }).catch(function (error) {
        console.log("Error: ", error);
    });

    this.$onInit = function () {
        this.service.setLanguage('fr');
    }

    this.sortBy = function () {
        if ($scope.sort === "option-1") {
            $scope.movies.sort((a, b) => a.Title.localeCompare(b.Title));
        } else if ($scope.sort === "option-2") {
            $scope.movies.sort((a, b) => a.Year - b.Year);
        } else if ($scope.sort === "option-3") {
            $scope.movies.sort((actualMovie, nextMovie) => nextMovie.AverageNote - actualMovie.AverageNote);
        }
    }
}]);

app.controller('movieController', ["$scope", "$route", "MovieService", "LanguageService", function ($scope, $route, MovieService, LanguageService) {
    $scope.service = LanguageService;

    var imdbID = $route.current.params.imdbID;

    MovieService.getMovieById(imdbID).then(function (movie) {
        $scope.movie = movie;
    }).catch(function (error) {
        console.log("Error: ", error);
    });
}]);