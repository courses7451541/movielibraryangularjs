﻿angular.module('movieLibraryApp')
    .component('commentsTemplate', {
        template: `
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Comments</h5>
                    <div style="max-height: 470px; overflow-y: scroll;">
                        <div ng-repeat="comment in comments" class="card mb-3">
                            <div class="card-body">
                                <p class="card-text">{{ comment.comment }}</p>
                            </div>
                        </div>
                    </div>
                    <form name='commentForm' ng-submit="$ctrl.addComment(comment)">
                        <div class="form-group">
                            <label for="comment">Leave a comment:</label>
                            <textarea class="form-control" id="comment" rows="2" ng-model="comment" required></textarea>
                        </div>
                        <button type="submit" ng-disabled="commentForm.$invalid" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        `,
        controller: function ($scope, $route, MovieService, LanguageService) {
            $scope.service = LanguageService;
            var imdbID = $route.current.params.imdbID;

            this.updateComments = function() {
                $scope.comments = MovieService.getComments(imdbID);
                console.log($scope.comments);
            }

            this.addComment = function(comment) {
                MovieService.addComment(imdbID, comment);
                console.log(imdbID, comment);
                this.updateComments();
            }

    
            this.$onInit = function () {
                $scope.comments = MovieService.getComments(imdbID);
            }
        }
    })