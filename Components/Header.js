angular.module('movieLibraryApp')
    .component('headerTemplate', {
        template: `
            <nav class="navbar bg-body-tertiary">
              <div class="container">
                <a class="navbar-brand" href="#/">{{$ctrl.service.translate('home')}}</a>
                <div>
                    <select ng-model="$ctrl.selectedLanguage" ng-change="$ctrl.service.setLanguage($ctrl.selectedLanguage)">
                        <option value="fr">Français</option>
                        <option value="en">English</option>
                        <option value="de">Deutsch</option>
                    </select>
                    <label>{{$ctrl.service.translate('welcome')}} {{$ctrl.loginName}}</label>
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#signinModal" ng-if="$ctrl.loginName === ''">{{$ctrl.service.translate('signInOrSignUp')}}</button>
                </div>
              </div>
            </nav>
            <div id="signinModal" aria-labelledby="signinModalLabel" class="modal" tabindex="-1" ng-if="$ctrl.loginName === ''">
              <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 id="signinModalLabel" class="modal-title">{{$ctrl.service.translate('loginForm')}}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    <form name="userForm" novalidate ng-submit="$ctrl.login()">
                        <label class="form-label">{{$ctrl.service.translate('username')}} :</label>
                        <input type="text" class="form-control mb-4" name="username" ng-model="user.name" required placeholder="{{$ctrl.service.translate('inputYourUsername')}}" aria-label="Username" aria-describedby="basic-addon1">
                        <p ng-show="userForm.username.$error.required && !userForm.username.$pristine">{{$ctrl.service.translate('usernameRequired')}}</p>
                        
                        <label class="form-label">{{$ctrl.service.translate('password')}} :</label>
                        <input type="password" class="form-control mb-3" placeholder="{{$ctrl.service.translate('inputYourPassword')}}" aria-label="Password" aria-describedby="basic-addon1">
                        <p ng-show="userForm.password.$error.required && !userForm.password.$pristine">{{$ctrl.service.translate('passwordRequired')}}</p>
                        
                        <p>{{$ctrl.service.translate('dontHaveAnAccount')}} <a class="text-primary-emphasis" data-bs-toggle="modal" data-bs-target="#signupModal">{{$ctrl.service.translate('signUp')}}</a></p>
                        
                        <div class="modal-footer">
                            <button id="loginOrRegistration" type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{$ctrl.service.translate('close')}}</button>
                            <button type="submit" ng-disabled="userForm.$invalid" class="btn btn-primary">{{$ctrl.service.translate('signIn')}}</button>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div id="signupModal" aria-labelledby="signupModalLabel" class="modal" tabindex="-1" ng-if="$ctrl.loginName === ''">
              <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 id="signupModalLabel" class="modal-title">{{$ctrl.service.translate('registerForm')}}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="{{$ctrl.service.translate('close')}}"></button>
                  </div>
                  <div class="modal-body">
                    <form name="userForm2" novalidate ng-submit="$ctrl.authoService.register(user)">
                        <label class="form-label">{{$ctrl.service.translate('username')}}:</label>
                        <input type="text" class="form-control mb-4" name="username" ng-model="user.name" required placeholder="{{$ctrl.service.translate('inputYourUsername')}}" aria-label="Username" aria-describedby="basic-addon1">
                        <p ng-show="userForm2.username.$error.required && !userForm2.username.$pristine">{{$ctrl.service.translate('usernameRequired')}}</p>
                        
                        <label class="form-label">{{$ctrl.service.translate('password')}}:</label>
                        <input type="password" ng-model="user.password" class="form-control mb-3" placeholder="{{$ctrl.service.translate('inputYourPassword')}}" aria-label="Password" aria-describedby="basic-addon1">
                        <p ng-show="userForm2.password.$error.required && !userForm2.password.$pristine">{{$ctrl.service.translate('passwordRequired')}}</p>
                        
                        <p>{{$ctrl.service.translate('doHaveAnAccount')}} <a class="text-primary-emphasis" data-bs-toggle="modal" data-bs-target="#signinModal">{{$ctrl.service.translate('signIn')}}</a></p>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{$ctrl.service.translate('close')}}</button>
                            <button type="submit" ng-disabled="userForm2.$invalid" class="btn btn-primary">{{$ctrl.service.translate('signUp')}}</button>
                        </div>
                    </form>
                  </div>
                  
                </div>
              </div>
            </div>
        `,
        controller: function ($scope, LanguageService, AuthenticationService) {
            $scope.user = {};
            this.service = LanguageService;
            this.authoService = AuthenticationService;
            this.loginName = '';

            this.$onInit = function () {
                this.service.setLanguage('fr');
                this.selectedLanguage = 'fr';
            };

            this.login = function () {
                if(this.authoService.login($scope.user.name, $scope.user.password)) {
                    document.getElementById('loginOrRegistration').click();
                    this.loginName = this.authoService.getActualUser().name;
                }
            }
        }
    })