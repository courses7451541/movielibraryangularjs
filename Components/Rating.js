﻿angular.module('movieLibraryApp')
    .component('ratingTemplate', {
        template: `
            <div class="card-body">
                <h5 class="card-title">Rating</h5>
                <p>Average Rating: {{ $ctrl.averageNote }}/5</p>
                <form name='ratingForm' ng-submit="$ctrl.addNote(note)">
                    <div class="form-group">
                        <label for="rating">Rate the movie (0-5):</label>
                        <input type="number" class="form-control" id="rating" min="0" max="5" ng-model="note" required>
                    </div>
                    <button type="submit" ng-disabled="ratingForm.$invalid" class="btn btn-primary">Submit</button>
                </form>
            </div>
        `,
        controller: function ($scope, $route, MovieService, LanguageService) {
            $scope.service = LanguageService;
            var imdbID = $route.current.params.imdbID;

            this.updateNotes = function() {
                $scope.notes = MovieService.getNotes(this.imdbID);
                console.log($scope.notes);
            };

            this.addNote = function(note) {
                MovieService.addNote(imdbID, note);
                console.log(imdbID, note);
                this.updateNotes();
            };
        
            this.$onInit = function () {
                this.updateNotes();
                this.getAverage(imdbID)
            };

            this.getAverage = function (imdbID) {
                let notes = MovieService.getNotes(imdbID);
                let total = 0;
                let count = 0;
                notes.forEach(function (note) {
                    total += note.note;
                    count++;
                });
                this.averageNote = (total/count).toFixed(2);
            }
        }
    })