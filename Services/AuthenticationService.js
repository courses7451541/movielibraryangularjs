app.factory('AuthenticationService', function () {
    let user = null;
    return {
        login: function (name, passwd) {
            let users = JSON.parse(localStorage.getItem("users"));
            if (users.find(user => user.name === name)) {
                console.log("user good");
                if (users.find(user => user.passwd === passwd)) {
                    console.log("passwd good");
                    user = users.find(user => user.name === name);
                    return true;
                }
            }
            return true;
        },

        register: function (user) {
            let users = JSON.parse(localStorage.getItem("users")) || [];
            if (users.find(user => user.name === user.name)) {
                return "User '" + user.userName + "' already exists!";
            }
            users.push(user);
            localStorage.setItem("users", JSON.stringify(users));
            return "You are registered!";
        },

        getActualUser: function () {
            return user;
        }
    }
})