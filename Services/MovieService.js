app.factory('MovieService', function ($http) {
    return {
        getMovieById: function (id) {
            return $http({
                method: 'GET',
                url: "http://www.omdbapi.com/?i=" + id + "&apikey=a4157692"
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                return response;
            });
        },

        getMovieByTitle: function (title) {
            return $http({
                method: 'GET',
                url: "http://www.omdbapi.com/?t=" + title + "&apikey=a4157692"
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                return response;
            });
        },

        getAllMovies: function () {
            return $http({
                method: 'GET',
                url: "http://www.omdbapi.com/?s=star&apikey=a4157692"
            }).then(function successCallback(response) {
                return response.data.Search;
            }, function errorCallback(response) {
                return response;
            })
        },

        getComments: function (movieId) {
            return JSON.parse(localStorage.getItem(movieId + "Comment"));
        },

        addComment: function (movieId, comment) {
            let fullComment = {
                comment: comment
            }
            let comments = this.getComments(movieId);
            if (comments == null) {
                comments = [fullComment];
            } else {
                comments = [...comments, fullComment];
            }
            localStorage.setItem(movieId + "Comment", JSON.stringify(comments));
        },

        deleteComment: function (movieId, comment) {
            let comments = this.getComments(movieId);
            comments = comments.filter(function(c) {
                return c !== comment;
            });
            localStorage.setItem(movieId + "Comment", JSON.stringify(comments));
        },


        getNotes: function (movieId) {
            return JSON.parse(localStorage.getItem(movieId + "Note"));
        },

        addNote: function (movieId, note, userName) {
            let notes = JSON.parse(localStorage.getItem(movieId + "Note")) || [];
            notes.push({note: note, userName: userName});
            localStorage.setItem(movieId + "Note", JSON.stringify(notes));
            return "The note " + note + " has been added.";
        },

        deleteNote: function (movieId, note) {
            let notes = this.getNotes(movieId);
            notes = notes.filter(function(n) {
                return n.note !== note;
            });
            localStorage.setItem(movieId + "Note", JSON.stringify(notes));
            return "The note " + note + " has been deleted.";
        }
    }
});