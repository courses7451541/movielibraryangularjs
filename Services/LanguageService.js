app.factory('LanguageService', function () {
    let dataLang = [
        {
            lang: 'fr',
            data: {
                "home" : "Accueil",
                "signInOrSignUp" : "Connexion / Inscription",
                "loginForm" : "Formulaire de connexion",
                "registerForm" : "Formulaire d'inscription",
                "username" : "Nom d'utilisateur",
                "password" : "Mot de passe",
                "usernameRequired" : "Le nom d'utilisateur est requis.",
                "passwordRequired" : "Le mot de passe est requis.",
                "dontHaveAnAccount" : "Vous n'avez pas de compte? ",
                "doHaveAnAccount" : "Vous avez déjà un compte? ",
                "signIn" : "Se connecter",
                "signUp" : "S'inscrire",
                "close" : "Fermer",
                "inputYourUsername" : "Entrez votre nom d'utilisateur",
                "inputYourPassword" : "Entrez votre mot de passe",
                "year" : "Année",
                "showMore" : "Voir plus",
                "alphabeticOrder" : "Ordre alphabétique",
                "sortBy" : "Trier par",
                "marks" : "Notes",
                "ChooseOne" : "Choisissez une option",
            }
        },{
            lang: 'en',
            data: {
                "home" : "Home",
                "signInOrSignUp" : "Sign In / Sign Up",
                "loginForm" : "Login Form",
                "registerForm" : "Registration Form",
                "username" : "Username",
                "password" : "Password",
                "usernameRequired" : "Username is required.",
                "passwordRequired" : "Password is required.",
                "dontHaveAnAccount" : "Don't have an account? ",
                "doHaveAnAccount" : "Already have an account? ",
                "signIn" : "Sign In",
                "signUp" : "Sign Up",
                "close" : "Close",
                "inputYourUsername" : "Input your username",
                "inputYourPassword" : "Input your password",
                "year" : "Year",
                "showMore" : "Show more",
                "alphabeticOrder" : "Alphabetic order",
                "sortBy" : "Sort by",
                "marks" : "Marks",
                "ChooseOne" : "Choose one option",
            }
        }, {
            lang: 'de',
            data: {
                "home" : "Zuhause",
                "signInOrSignUp" : "Anmelden / Registrieren",
                "loginForm" : "Anmeldeformular",
                "registerForm" : "Registrierungsformular",
                "username" : "Nutzername",
                "password" : "Passwort",
                "usernameRequired" : "Benutzername ist erforderlich.",
                "passwordRequired" : "Passwort ist erforderlich.",
                "dontHaveAnAccount" : "Sie haben noch kein Konto? ",
                "doHaveAnAccount" : "Sie haben bereits ein Konto? ",
                "signIn" : "Anmelden",
                "signUp" : "Anmelden",
                "close" : "Schließen",
                "inputYourUsername" : "Geben Sie Ihren Benutzernamen ein",
                "inputYourPassword" : "Geben Sie Ihr Passwort ein",
                "year" : "Jahr",
                "showMore" : "Mehr anzeigen",
                "alphabeticOrder" : "Alphabetische Reihenfolge",
                "sortBy" : "Sortieren nach",
                "marks" : "Noten",
                "ChooseOne" : "Wählen Sie eine Option",
            }
        }
    ]
    return {

        setLanguage: function (language) {
            localStorage.setItem('lang', language);
        },

        translate: function (stringToTranslate) {
            return dataLang.find(language => language.lang === localStorage.getItem('lang')).data[stringToTranslate];
        }
    }
});