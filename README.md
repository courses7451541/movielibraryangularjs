# Movie Library Angularjs

Requirements:

- [X] Gestionnaire de liste de films
- [X] Affichage donc du titre, courte description, note moyenne (et nombre de notes)
- [X] Possibilité de classer les films par ordre alphabétique, ou par note moyenne (mais 5/5 à une note, moins bien que
  4/5 à 100 notes !)
- [X] Possibilité de se connecter
- [X] Si connecté, possibilité d’ajouter des notes, et / ou de commenter
- [X] Données persistantes
- [X] SPA

Bonus:

- [X] en cas d’intégration d’une API qui permet de fetch sur google la cover des films
- [ ] en cas de fonctionnalité de recherche dans la liste
- [X] en cas de localisation (3 langues minimum)
- [X] en cas d’utilisation d’API pour fetch une grande liste de films

By Tony, Paul and Julien